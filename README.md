# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Pavel Semikolenov
* **E-MAIL**: pavelsemikolenov@yandex.ru

## SOFTWARE

* OpenJDK 8

* Intellij Idea

* MS Windows 10

## HARDWERE

* **RAM**: 14Gb
* **CPU**: i5
* **HDD**: 512Gb

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
